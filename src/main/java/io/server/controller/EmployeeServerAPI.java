package io.server.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.server.model.EmpList;
import io.server.model.Employee;


@RestController
@RequestMapping(value="/")
public class EmployeeServerAPI {
	
	List<Employee> empList=new ArrayList<Employee>();
	
	public EmployeeServerAPI()
	{
		Employee emp1=new Employee();
		emp1.setId(1);
		emp1.setName("Harry");
		emp1.setGender(Employee.Gender.MALE);
		
		Employee emp2=new Employee();
		emp2.setId(2);
		emp2.setName("Joe");
		emp2.setGender(Employee.Gender.FEMALE);
		
		Employee emp3=new Employee();
		emp3.setId(3);
		emp3.setName("Tom");
		emp3.setGender(Employee.Gender.MALE);
		
		Employee emp4=new Employee();
		emp4.setId(4);
		emp4.setName("Shweta");
		emp4.setGender(Employee.Gender.FEMALE);
		
		Employee emp5=new Employee();
		emp5.setId(5);
		emp5.setName("Vikram");
		emp5.setGender(Employee.Gender.MALE);
		
		Employee emp6=new Employee();
		emp6.setId(6);
		emp6.setName("Monika");
		emp6.setGender(Employee.Gender.FEMALE);
		
		Employee emp7=new Employee();
		emp7.setId(7);
		emp7.setName("Vivek");
		emp7.setGender(Employee.Gender.MALE);
		
		Employee emp8=new Employee();
		emp8.setId(8);
		emp8.setName("amisha");
		emp8.setGender(Employee.Gender.FEMALE);
		
		Employee emp9=new Employee();
		emp9.setId(9);
		emp9.setName("Himanshu");
		emp9.setGender(Employee.Gender.MALE);
		
		Employee emp10=new Employee();
		emp10.setId(10);
		emp10.setName("shreya");
		emp10.setGender(Employee.Gender.FEMALE);
		
		
		empList.add(emp1);
		empList.add(emp2);
		empList.add(emp3);
		empList.add(emp4);
		empList.add(emp5);
		empList.add(emp6);
		empList.add(emp7);
		empList.add(emp8);
		empList.add(emp9);
		empList.add(emp10);
		
		
	}
	
	
	
	@RequestMapping(value="GET/test-server/v1/get-employees",method=RequestMethod.GET)
	public EmpList getAllEmployees() throws Exception{
		
		EmpList eList=new EmpList();
		
		eList.setEmpList(empList);
		return eList;
		
		
		
	}
	
	@RequestMapping(value="POST/test-server/v1/employee",method=RequestMethod.POST)
	public ResponseEntity<String> addEmployee(@RequestBody Employee emp) throws Exception{
		
		
		
		
		//EmployeeServerAPI e=new EmployeeServerAPI();
		//EmpList empList=e.getAllEmployees();
		
		//List<Employee> employeeList=empList.getEmpList();
		
		System.out.println("ddfd");
		empList.add(emp);
		
		for(Employee item:empList)
		{
			System.out.println(item.getId()+" "+item.getName()+" "+item.getGender());
		}
		
		
		String s="Employee successfully added with employee id "+Integer.toString(emp.getId());
		
		ResponseEntity<String> response=new ResponseEntity<String>(s,HttpStatus.CREATED);
		return response;
		
		
	}
	
	
	@RequestMapping(value="PUT/test-server/v1/{id}",method=RequestMethod.PUT)
	public ResponseEntity<String> updateEmployee(@PathVariable Integer id,@RequestBody Employee emp)
	{
		for(Employee item:empList)
		{
			if(item.getId()==id)
			{
				item.setId(emp.getId());
				item.setName(emp.getName());
				item.setGender(emp.getGender());
			}
			
		}
		
		for(Employee item:empList)
		{
			System.out.println(item.getId()+" "+item.getName()+" "+item.getGender());
		}
		
		String msg="Employee updated successfully with employee id "+id;
		ResponseEntity<String> response = new ResponseEntity<String>(msg, HttpStatus.OK);
		return response;
		
		
		
			
	}
	
	
	
	
	

}
