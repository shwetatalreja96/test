package io.server.model;


public class Employee {
	
	
	public enum Gender {
	    MALE,
	    FEMALE
	}

	
	private int id;
	private String name;
	public Gender gender;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	
	
	

}
