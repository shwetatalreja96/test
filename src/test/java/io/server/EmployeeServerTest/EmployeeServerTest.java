package io.server.EmployeeServerTest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.server.controller.EmployeeServerAPI;
import io.server.model.Employee;



@RunWith(SpringJUnit4ClassRunner.class) 
public class EmployeeServerTest {
	
	
private MockMvc mockMvc;
	
	@InjectMocks
	private EmployeeServerAPI esc;
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	public String objectToJSON(Object obj) throws JsonProcessingException{
		ObjectMapper mapper=new ObjectMapper();
		String JSON=mapper.writeValueAsString(obj);
		return JSON;
	}
	
	
	@Test
	public void testGetEmployee() throws Exception{
		mockMvc = MockMvcBuilders.standaloneSetup(esc).build();
		mockMvc.perform(get("/GET/test-server/v1/get-employees")
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.empList").isArray())
			.andExpect(jsonPath("$.empList[0].id",is(1)))
			.andExpect(jsonPath("$.empList[0].name",is("Harry")))
			.andExpect(jsonPath("$.empList[0].gender",is("MALE")));
	
	}
	
	
	
	@Test
	public void testPostEmployee() throws Exception{
		
		Employee e=new Employee();
		e.setId(11);
		e.setName("Garima");
		e.setGender(e.gender.FEMALE);
		
		String json=objectToJSON(e);
		mockMvc = MockMvcBuilders.standaloneSetup(esc).build();
		mockMvc.perform(post("/POST/test-server/v1/employee")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isCreated());
	}
	
	
	@Test
	public void testPutEmployee() throws Exception{
		Employee e=new Employee();
		e.setId(1);
		e.setName("Sachin");
		e.setGender(e.gender.MALE);
		String json=objectToJSON(e);
		
		mockMvc = MockMvcBuilders.standaloneSetup(esc).build();
		mockMvc.perform(put("/PUT/test-server/v1/{id}",1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
		.andExpect(status().isOk());
				
				
		
	}
	
	
}
